import React from 'react';
import { Table, Container } from 'reactstrap';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

const ListFacts = ({ limit, facts }) => {
    return (
        <Container>
            <Table>
                <thead>
                    <tr>
                        <th>Fact</th>
                        <th>Date</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {
                    facts.slice(0, limit).map(fact => (
                        <tr key = {fact._id}>
                            <td>{fact.text}</td>
                            <td>{fact.createdAt}</td>
                            <td className="text-right">
                                <Link to={`/EditFact/${fact._id}`}>Edit</Link>
                            </td>
                        </tr>
                    ))
                }
                </tbody>
            </Table>
        </Container>
    )
}

ListFacts.propTypes = {
    limit: PropTypes.number
}

export default ListFacts;